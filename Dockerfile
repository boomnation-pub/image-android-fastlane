FROM reactnativecommunity/react-native-android:6.1

ENV FASTLANE_VERSION 2.197.0

# Installing build tools
RUN apt-get update && \
  apt-get install -y \
  build-essential \
  ruby \
  ruby-dev

# Install imagemagick
RUN apt-get install -y libtool libyaml-dev imagemagick

# Installing fastlane
RUN gem install fastlane -v ${FASTLANE_VERSION} \
  && gem install fastlane-plugin-appicon fastlane-plugin-android_change_string_app_name fastlane-plugin-humanable_build_number \
  && gem update --system

# Install aws cli
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
    && unzip awscliv2.zip \
    && ./aws/install

# Remove Build Deps
RUN apt-get purge -y --auto-remove $buildDeps

# Output versions
RUN node -v && npm -v && ruby -v && fastlane -v && aws --version && java --version
